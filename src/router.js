import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home.vue'
import ChatbotsList from './views/ChatbotsList.vue'
import ContactProfile from './views/ContactProfile.vue'

Vue.use(Router);

const routes = [
      { 
        path: '/home',
        name: 'home',
        component: Home,
        meta: {
          title: 'Home'
        }
    },
    { 
        path: '/',
        name: 'chatbots_list',
        component: ChatbotsList,
        meta: {
          title: 'Chatbots List'
        }
    },
    { 
        path: '/profile/:id',
        name: 'contact_profile',
        component: ContactProfile,
        meta: {
          title: 'Contact Profile'
        }
    }
];

const router = new Router({
    mode: 'history',
    routes: routes
})

export default router;